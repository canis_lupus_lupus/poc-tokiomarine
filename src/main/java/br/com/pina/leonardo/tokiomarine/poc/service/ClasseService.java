package br.com.pina.leonardo.tokiomarine.poc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import br.com.pina.leonardo.tokiomarine.poc.entity.Classe;
import br.com.pina.leonardo.tokiomarine.poc.repository.ClasseRespository;

@Service
public class ClasseService {
	
	@Autowired
	private ClasseRespository repository;
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	public ClasseService() {
	}
	
	public boolean salvarClasse(Classe classe) {
		
		jmsTemplate.convertAndSend("classe.inbound", classe);
		
		try {
			repository.save(classe);
			return true;
		} catch (DataAccessException e) {
			return false;
		}
	}
}
