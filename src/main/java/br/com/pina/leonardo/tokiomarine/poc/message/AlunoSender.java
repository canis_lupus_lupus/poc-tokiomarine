package br.com.pina.leonardo.tokiomarine.poc.message;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import br.com.pina.leonardo.tokiomarine.poc.entity.Aluno;

@Component
public class AlunoSender {

	@Autowired
	JmsTemplate jmsTemplate;

	public void sendMessage(final String queueName, final Aluno aluno) {

		System.out.println("Sending aluno " + aluno + "to queue - " + queueName);
		jmsTemplate.send(queueName, new MessageCreator() {

			public Message createMessage(Session session) throws JMSException {
				TextMessage message = session.createTextMessage();
				return message;
			}
		});
	}
	
}
