package br.com.pina.leonardo.tokiomarine.poc.message;

import org.springframework.jms.annotation.JmsListener;

import br.com.pina.leonardo.tokiomarine.poc.entity.Aluno;

public class AlunoReciever {

	@JmsListener(destination = "aluno.inbound", containerFactory = "myFactory")
	public void receiveMessage(Aluno aluno) {
		System.out.println("Received <" + aluno + ">");
	}
}
