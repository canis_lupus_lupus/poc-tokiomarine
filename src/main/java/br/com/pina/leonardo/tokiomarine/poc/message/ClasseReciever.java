package br.com.pina.leonardo.tokiomarine.poc.message;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.pina.leonardo.tokiomarine.poc.entity.Classe;

@Component
public class ClasseReciever {

	@JmsListener(destination = "classe.inbound", containerFactory = "myFactory")
	public void receiveMessage(Classe classe) {
		System.out.println("Received <" + classe.getNome() + ">");
	}

}
