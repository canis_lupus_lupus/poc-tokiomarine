package br.com.pina.leonardo.tokiomarine.poc.service;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pina.leonardo.tokiomarine.poc.entity.Aluno;
import br.com.pina.leonardo.tokiomarine.poc.entity.Classe;

@Service
public class ApplicationService {

	@Autowired
	private AlunoService alunoService;

	@Autowired
	private ClasseService classeService;
	
	public ApplicationService() {
		// TODO Auto-generated constructor stub
	}

	private List<Aluno> createAlunos() {

		return Arrays.asList(new Aluno("Pedro"), new Aluno("Camila"), new Aluno("Leandro"), new Aluno("Emanuel"),
				new Aluno("Lara"), new Aluno("Tatiana"), new Aluno("Juliana"));
	}

	private List<Classe> createClasses() {

		return Arrays.asList(new Classe("8serie"), new Classe("7serie"), new Classe("6serie"), new Classe("5serie"),
				new Classe("4serie"), new Classe("3serie"), new Classe("2serie"));
	}

	private Aluno assignClass(Aluno aluno, Classe classe) {
		aluno.setClasse(classe);

		return aluno;
	}

	public boolean cadastrarElementosIniciais() {
		Random r = new Random();
		List<Aluno> alunos = createAlunos();
		List<Classe> classes = createClasses();
		int s = alunos.size();

		classes.forEach(c -> classeService.salvarClasse(c));

		alunos.forEach((a) -> {
			assignClass(a, classes.get(r.nextInt(s - 1)));
			alunoService.salvarAluno(a);
		});

		return true;

	}

}
