package br.com.pina.leonardo.tokiomarine.poc.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pina.leonardo.tokiomarine.poc.entity.Classe;

@Transactional
public interface ClasseRespository extends JpaRepository<Classe, Integer>{

}
