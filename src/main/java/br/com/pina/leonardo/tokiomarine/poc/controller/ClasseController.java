package br.com.pina.leonardo.tokiomarine.poc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.pina.leonardo.tokiomarine.poc.entity.Classe;
import br.com.pina.leonardo.tokiomarine.poc.service.ClasseService;

@Controller
@RequestMapping("/classe")
public class ClasseController {

	@Autowired
	private ClasseService classeService;

	@RequestMapping(value = "/salvar", method = RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> SalvarClasse(@RequestParam Classe classe) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

		if (classeService.salvarClasse(classe)) {
			status = HttpStatus.OK;
		}

		return new ResponseEntity<>(status);
	}
}
