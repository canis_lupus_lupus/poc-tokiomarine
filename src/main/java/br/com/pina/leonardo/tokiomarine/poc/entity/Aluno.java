package br.com.pina.leonardo.tokiomarine.poc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Aluno {

	public Aluno(String nome) {
		this.nome = nome;
	}

	public Aluno() {
	}
	
	@Override
	public String toString() {
		
		return this.nome + (this.classe != null ? " - " + this.classe.getNome() : " - sem classe ");
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(nullable = false)
	private String nome;
	@ManyToOne
	private Classe classe;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public int getId() {
		return id;
	}

}
