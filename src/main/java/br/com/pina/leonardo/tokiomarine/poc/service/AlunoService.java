package br.com.pina.leonardo.tokiomarine.poc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import br.com.pina.leonardo.tokiomarine.poc.entity.Aluno;
import br.com.pina.leonardo.tokiomarine.poc.repository.AlunoRepository;

@Service
public class AlunoService {

	@Autowired
	private AlunoRepository repository;
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	public AlunoService() {
	}
	
	public boolean salvarAluno(Aluno aluno) {
		
		jmsTemplate.convertAndSend("aluno.inbound", aluno);
		
		try{
			repository.save(aluno);
			return true;
		} catch (DataAccessException e) {
			return false;
		}
	}
}
