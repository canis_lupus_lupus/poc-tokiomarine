INSERT INTO classe(nome) VALUES ('1A'); 
INSERT INTO classe(nome) VALUES ('1B'); 
INSERT INTO classe(nome) VALUES ('2A'); 
INSERT INTO classe(nome) VALUES ('2B'); 
INSERT INTO classe(nome) VALUES ('2C'); 
INSERT INTO classe(nome) VALUES ('2D'); 
INSERT INTO classe(nome) VALUES ('3A'); 
INSERT INTO classe(nome) VALUES ('3B'); 

insert into aluno(nome, classe_id) values (
	'Jose', 
	select id from classe where nome = '1A');
	
insert into aluno(nome, classe_id) values (
	'Aguinaldo', 
	select id from classe where nome = '1A');
	
insert into aluno(nome, classe_id) values (
	'Matias', 
	select id from classe where nome = '2A');
	
insert into aluno(nome, classe_id) values (
	'Maria', 
	select id from classe where nome = '1B');
	
insert into aluno(nome, classe_id) values (
	'Timoteo', 
	select id from classe where nome = '3A');
	
insert into aluno(nome, classe_id) values (
	'Valeria', 
	select id from classe where nome = '1B');
	
insert into aluno(nome, classe_id) values (
	'Francine', 
	select id from classe where nome = '2A');
	
insert into aluno(nome, classe_id) values (
	'Luisa', 
	select id from classe where nome = '2C');
	
insert into aluno(nome, classe_id) values (
	'Fernando', 
	select id from classe where nome = '2D');
	
insert into aluno(nome, classe_id) values (
	'Marta', 
	select id from classe where nome = '2D');
	
insert into aluno(nome, classe_id) values (
	'Marcos', 
	select id from classe where nome = '1A');
	
insert into aluno(nome, classe_id) values (
	'Paulo', 
	select id from classe where nome = '2A');
	
insert into aluno(nome, classe_id) values (
	'Augusto', 
	select id from classe where nome = '2A');
	
insert into aluno(nome, classe_id) values (
	'Joao', 
	select id from classe where nome = '3A');
	
insert into aluno(nome, classe_id) values (
	'Nathalia', 
	select id from classe where nome = '3B');
	
insert into aluno(nome, classe_id) values (
	'Guilherme', 
	select id from classe where nome = '3B');
	
insert into aluno(nome, classe_id) values (
	'Fernanda', 
	select id from classe where nome = '2C');
	
insert into aluno(nome, classe_id) values (
	'Raquel', 
	select id from classe where nome = '2B');
	
insert into aluno(nome, classe_id) values (
	'Martino', 
	select id from classe where nome = '1B');